package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.AbstractImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;

public class WalletFrame extends AbstractImageFrameMedia<RandomAccessFile> implements WalletFrameMedia, AutoCloseable {

    private BufferedReader encodedImageReader;
    private FileOutputStream encodedImageOutput = null;
    private long numberOfLines;

    public WalletFrame(RandomAccessFile walletDatabase) {
        super(walletDatabase);
    }

    @Override
    public OutputStream getEncodedImageOutput() throws IOException {
        if (encodedImageOutput == null){
            RandomAccessFile file = getChannel();
            this.encodedImageOutput = new FileOutputStream(file.getFD());
            long fileLength = file.length();

            if(fileLength == 0) {
                // Ecriture de l'amorce
                Writer writer = new PrintWriter(encodedImageOutput, true, StandardCharsets.UTF_8);
                writer.write(MessageFormat.format("{0,number,#};{1,number,#};", 1, file.getFilePointer()));
                writer.flush();
            }
            else{
                BufferedReader br = getEncodedImageReader(false);
                String lastLine = MetadataDeserializerDatabaseImpl.readLastLine(file);
                String[]data = lastLine.split(";");
                this.numberOfLines = Long.parseLong(data[0]) - 1;
                file.seek(fileLength);
            }
        }
        return encodedImageOutput;
    }

    @Override
    public InputStream getEncodedImageInput() throws IOException {
        return new BufferedInputStream(new FileInputStream(getChannel().getFD()));
    }

    @Override
    public BufferedReader getEncodedImageReader(boolean resume) throws IOException {
        if (encodedImageReader == null || !resume){
            this.encodedImageReader = new BufferedReader(new InputStreamReader(new FileInputStream(getChannel().getFD()), StandardCharsets.UTF_8));
        }
        return this.encodedImageReader;
    }

    @Override
    public long getNumberOfLines() {
        return numberOfLines;
    }

    @Override
    public void incrementLines() {
        this.numberOfLines++;
    }

    /**
     * Closes this resource, relinquishing any underlying resources.
     * This method is invoked automatically on objects managed by the
     * {@code try}-with-resources statement.
     *
     * @throws Exception if this resource cannot be closed
     * @apiNote While this interface method is declared to throw {@code
     * Exception}, implementers are <em>strongly</em> encouraged to
     * declare concrete implementations of the {@code close} method to
     * throw more specific exceptions, or to throw no exception at all
     * if the close operation cannot fail.
     *
     * <p> Cases where the close operation may fail require careful
     * attention by implementers. It is strongly advised to relinquish
     * the underlying resources and to internally <em>mark</em> the
     * resource as closed, prior to throwing the exception. The {@code
     * close} method is unlikely to be invoked more than once and so
     * this ensures that the resources are released in a timely manner.
     * Furthermore it reduces problems that could arise when the resource
     * wraps, or is wrapped, by another resource.
     *
     * <p><em>Implementers of this interface are also strongly advised
     * to not have the {@code close} method throw {@link
     * InterruptedException}.</em>
     * <p>
     * This exception interacts with a thread's interrupted status,
     * and runtime misbehavior is likely to occur if an {@code
     * InterruptedException} is {@linkplain Throwable#addSuppressed
     * suppressed}.
     * <p>
     * More generally, if it would cause problems for an
     * exception to be suppressed, the {@code AutoCloseable.close}
     * method should not throw it.
     *
     * <p>Note that unlike the {@link Closeable#close close}
     * method of {@link Closeable}, this {@code close} method
     * is <em>not</em> required to be idempotent.  In other words,
     * calling this {@code close} method more than once may have some
     * visible side effect, unlike {@code Closeable.close} which is
     * required to have no effect if called more than once.
     * <p>
     * However, implementers of this interface are strongly encouraged
     * to make their {@code close} methods idempotent.
     */
    @Override
    public void close() throws Exception {

    }
}
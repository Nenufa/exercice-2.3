package fr.cnam.foad.nfa035.badges.wallet.model;


/**
 *
 */
public class DigitalBadgeMetadata extends DigitalBadge {

    private int badgeId;
    private long imageSize;
    private long walletPosition;

    public DigitalBadgeMetadata(){};

    public void equals() {
        super.equals();
    }

    public int getBadgeId() {
        return badgeId;
    }

    public long getImageSize() {
        return imageSize;
    }

    public long getWalletPosition() {
        return walletPosition;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public void setBadgeId(){

    };

    public void setImageSize(){

    }

    public void setWalletPosition(){

    }

    @Override
    public String toString() {
        return super.toString();
    }
}

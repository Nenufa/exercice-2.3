package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.RandomAccessFile;

/**
 *
 */
public interface WalletFrameMedia extends ResumableImageFrameMedia<RandomAccessFile> {
    long getNumberOfLines();
    void incrementLines();
}
